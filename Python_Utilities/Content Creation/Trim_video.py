from moviepy.editor import *
import os

infile = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\Masking\\all_around_old.mp4"
outfile = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\Masking\\test.mp4"

sectionToRemove = [0,220] # 23 start and end (s)

invideo = VideoFileClip(infile) # input video
outvideo = invideo.cutout(sectionToRemove[0],sectionToRemove[1])

outvideo.write_videofile(outfile,audio=False) # ,fps=invideo.fps,audio=False,codec ="libx264",audio_bitrate="128k")