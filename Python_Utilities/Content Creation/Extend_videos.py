from moviepy.editor import *
import os

inpath = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\asl"
outpath = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\asl_ext"

if not os.path.isdir(outpath):
    os.mkdir(outpath)
    print("Made folder ",outpath)

freezeDuration = 1.5 # freeze first frame for 1.5 seconds 

for infile in os.listdir(inpath):
    outfile = os.path.join(outpath,os.path.splitext(infile)[0] + ".avi")
    if not os.path.exists(outfile):
        invideo = VideoFileClip(os.path.join(inpath, infile)) # input video
        frame1 = invideo.to_ImageClip(0) # first frame
        outvideo = concatenate_videoclips([frame1.set_duration(1.5),invideo])
        outvideo.write_videofile(outfile,fps=invideo.fps,codec = "mpeg4",audio_bitrate="192k")