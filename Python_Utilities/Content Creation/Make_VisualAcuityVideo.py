from moviepy.editor import *
from imageio import imread
import numpy as np
import os

infile = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\VisualAcuity\\ETDRS_VisualAcuityChart_1080p.png" # Sample_Snellen_chart_small.png"
outfile = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Assets\\VisualAcuity\\ETDRS_VisualAcuityChart_1080p.mp4" # Sample_Snellen_chart.mp4"

duration = 3 # s

screen_w = 1920 # 1920 640
screen_h = 1080 # 1080 480
white = 255*np.ones([screen_h,screen_w,3]) # RGB matrix with white rectangle
fixedFrame = ImageClip(white) # white rectangle
img = ImageClip(infile) # input image (eye test chart)
h_range = screen_h//2-img.h//2+np.arange(img.h)
w_range = screen_w//2-img.w//2+np.arange(img.w)
for y in range(img.h):
    for x in range(img.w):
        for c in range(3): # colors
            fixedFrame.img[h_range[y],w_range[x],c] = img.img[y,x,c]

outvideo = fixedFrame.set_duration(duration)

outvideo.write_videofile(outfile,fps=30,codec = "mpeg4") # ,fps=invideo.fps,audio=False,codec ="libx264",audio_bitrate="128k")