import Test_classes as le
import os

root = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\" # change as appropriate


# Create an array of idling videos for the three screens. "_" Means blank.
idling = ["idle/Idle.mp4", "idle/Idle.mp4", "idle/Idle.mp4"] # we are using three speakers so we turn on all three screens

# Create a screens instance with a screen positions text file created using the relevant max patch utility and the
#  idling videos
screens = le.Screens(root + "Test_Files\\positions.txt", idling)

# Create three SentenceLists: the first one for the target speaker and the other two for the maskers.
# IMPORTANT: the same list_order must be used for all three.
n_lists = 4 # normally we have up to 18 lists, but this is a small test
list_order = [0, 0, 0, 0] # normally we have somthing like [0, 15, 30...] but we keep it simple here for the test
text_files = ["ASL_ext_short1","ASL_ext_short2","ASL_ext_short3"] # e.g. ["Thomas", "Allan", "Barry"]

target_sentences = le.SentenceList(n_lists, list_order, text_files[0])
masker_sentences_1 = le.SentenceList(n_lists, list_order, text_files[1])
masker_sentences_2 = le.SentenceList(n_lists, list_order, text_files[2])

# Put them all together in a list
sentences = [target_sentences, masker_sentences_1, masker_sentences_2]

# Gain in dB of onscreen maskers compared to background maskers (default = 0)
onscreen_gain = 10 

# Message that the experimenter should read out loud to the subject
info_msg = "Report any sentence that mentions 'furniture', 'table', 'head' or 'leaves'" 

# Create some arrays containing all the available babble files.
male_dir = root + "Media\\Masking\\male_babble"
male_babble = [os.path.join(male_dir, file) for file in os.listdir(male_dir)]
female_dir = root + "Media\\Masking\\female_babble"
female_babble = [os.path.join(female_dir, file) for file in os.listdir(female_dir)]

# Create a masking condition called "around" with 8 masking sources. Position each source. Add an audio file to each
# masking source from the array of babble files created before. Add a masking video
around = le.MaskingCondition("around", 8)
around.set_source_positions([[1, 45], [1, 45], [1, 135], [1, 135], [1, 225], [1, 225], [1, 315], [1, 315]])
around.set_audio_files([male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop(), male_babble.pop(), female_babble.pop()])
around.set_video("all_around.mp4")

# Create some Test conditions, all of which use the around masking condition. Define their condition code,
# audio only flag, spatialisation flag and include the screen positions. Finally add a description for each one.

# NOTE: the active screen is defined in the sentence list .txt files next to each sentence. If not defined there, of if
# the order is not correct (e.g. <1, >3 or target and masker playing from the same screen) a random screen order will be used

active_screen = 0 # NOTE: this parameter is included for compatibility with previous versions but has no effect
NSNV = le.TestCondition("NSNV", around, active_screen, 1, 0, screens)
NSNV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Three concurrent sources. No video, no spatialisation"
NSV = le.TestCondition("NSV", around, active_screen, 0, 0, screens)
NSV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Three concurrent sources. With video, no spatialisation"
SNV = le.TestCondition("SNV", around, active_screen, 1, 1, screens)
SNV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Three concurrent sources. No video, spatialised"
SV = le.TestCondition("SV", around, active_screen, 0, 1, screens)
SV.description = "Four babble sources at 45, 135, 225 and 315 degrees. Three concurrent sources. With video, spatialised"

# Define the default SNR increment.
SNR_increment = "10."

# Path to beep.wav file ("" means no beep)
beep = ""

# Whether to play maskers continuously
cont_mask = False

# Whether to report listening effort
ask_LE = True

# After how many sentences is listening effort reported (0=only at the end of a test condition)
n_LE = 0

# Change vector: the ith value indicates what happens to the SNR if i words are reported correctly
# -1 = SNR decreases by SNR_increment
# 0 = SNR is unchanged
# 1 = SNR increases by SNR_increment
# E.g. [1 1 1 0 -1 -1] means that with 0, 1 or 2 correct words SNR increases; with 3 words, it stays; with 4 or 5, it decreases
change_vector = [1, 1, -1, -1]

# Masker duration in seconds (def=3.52)
masker_duration = 3.52

# Whether to sync the background video with the audio playback
sync_background = True

# Create the test with the Test name, list of conditions, sentences used and the default SNR increment.
test_one = le.Test("Pilot One Multispeaker sync background", [NSNV, NSV, SNV, SV], sentences, SNR_increment, beep, cont_mask, ask_LE, n_LE, change_vector, masker_duration, onscreen_gain, info_msg, sync_background)

# Finally output the XML and JSON files
test_one.output()

