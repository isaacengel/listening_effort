import os
import csv
import matplotlib.pyplot as plt
import numpy as np

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

path = 'C:\\Users\\Craig\\Desktop\\Pilot results'
files = ["", ""], ["", ""], ["", ""], ["", ""]
for file in os.listdir(path):
    file_id = file.split('_')
    results = ("Log" in file_id[0])
    files[int(file_id[5]) - 1][results] = file

for participant_num, participant in enumerate(files):

    # read results file
    file = participant[0]
    csv_file = open(os.path.join(path, file))
    csv_reader = csv.reader(csv_file, delimiter=',')

    SNR_averages = []
    round_times = []
    prev_SNR = 0
    prev_round = 0
    sum = 0
    count = 0

    for row in csv_reader:
        # check there is information in the row - not the column labels or an event
        if row[0].isnumeric():

            round = int(row[0])
            SNR = float(row[2])
            SNR_jump = abs(prev_SNR - SNR)
            prev_SNR = SNR

            # if the SNR variation is 3dB start adding towards the SRT
            if SNR_jump == 3:
                sum += SNR
                count += 1

            # If there's a new round add the end of the round time to round times (used to plot SRTs across the whole round)
            # Also calculate SRT
            if round > prev_round:
                hr_m_s = row[5].split(':')
                time_s = int(hr_m_s[0]) * 60 * 60 + int(hr_m_s[1]) * 60 + int(hr_m_s[2])
                round_times.append(time_s)
                if prev_round > 0:
                    SNR_averages.append(sum / count)
                    sum = 0
                    count = 0
                prev_round += 1

    # last round SRT calc
    SNR_averages.append(sum / count)
    print(SNR_averages)

    # read log file
    file = participant[1]
    csv_file = open(os.path.join(path, file))
    csv_reader = csv.reader(csv_file, delimiter=',')

    reading = False
    data = []
    snr_avg_plot = []
    round_index = 0
    x = []
    x_index = 0

    for row in csv_reader:
        # Check for start and end events, only read data between these
        if len(row) > 7 and "start" in row[7]:
            reading = True
            continue
        if len(row) > 7 and "end" in row[7]:
            reading = False
            continue

        # Check the row is between start and end events and that it's not another event
        if isfloat(row[1]) and reading:
            time = row[0]
            hr_m_s = time.split(':')
            time_s = int(hr_m_s[0]) * 60 * 60 + int(hr_m_s[1]) * 60 + int(hr_m_s[2])

            # Check to see if the timestamp of the logs has surpassed the end time of the current round
            if round_index < len(round_times) - 1 and time_s > round_times[round_index + 1]:
                round_index += 1

            average = (float(row[4]) + float(row[5]))/2
            SNR = float(row[6])

            # Check to see pupil width is more than zero - less than this corresponds to someone's eyes being closed or
            # the vive losing track. If data is valid log the average pupil width of the two eyes, the SNR. Add to the
            # srt plot and just populate an x axis list.
            if average > 0:
                data.append([average, SNR])
                snr_avg_plot.append(SNR_averages[round_index])
                x.append(x_index)
                x_index += 1

    # Plotting graphs
    fig, ax1 = plt.subplots()
    data = np.array(data)
    # Smoothed pupil width
    window_size = 800 # four seconds at 200hz
    smoothed_pupilavg = smooth(data[:, 0], window_size)
    cropped_smoothed_pupilavg = smoothed_pupilavg[window_size:len(smoothed_pupilavg)-window_size]
    cropped_x = x[:len(smoothed_pupilavg) - window_size * 2]
    color = (1, 0.8, 0.8)
    ax1.plot(cropped_smoothed_pupilavg, color=color)

    # Poly fit for raw pupil width and axis labels
    poly = np.poly1d(np.polyfit(x, data[:, 0], 5))
    color = "tab:red"
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.set_ylabel('Pupil width', color=color)
    ax1.plot(cropped_x, poly(cropped_x), color=color)

    # SNR plot
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    snr = data[:, 1]
    cropped_snr = snr[window_size:len(smoothed_pupilavg) - window_size]
    color = (0.8, 0.8, 1)
    ax2.plot(cropped_snr, color=color)

    # SRT plot
    color = "tab:blue"
    ax2.set_ylabel('SNR', color=color)  # we already handled the x-label with ax1
    ax2.tick_params(axis='y', labelcolor=color)
    cropped_snr_avg_plot = snr_avg_plot[window_size:len(smoothed_pupilavg) - window_size]
    ax2.plot(cropped_snr_avg_plot, color=color)

    # save figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    fig.savefig("C:\\Users\\Craig\\Desktop\\Pilot Graph\\Participant_" + str(participant_num + 1), dpi=None, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format=None,
            transparent=False, bbox_inches=None, pad_inches=0.1,)