# testing the setup for Tim's student project
# no target sentences or videos
# only background video, which shows two people talking
# 2 "target" sounds are played from the two people's locations but they are actually set up as masker sources
# 1 babble noise behind the listener
# background video should start in sync with the 3 sound files
# no control over SNR, since we are not using actual targets or maskers
# only one trial per condition; this example has 2 conditions but more can be added in the same way

import Test_classes as le
import os

root = "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\" # change as appropriate

# General test settings
screens = le.Screens(root + "Test_Files\\positions.txt", ["_" , "_" , "_"]) # screens instance; irrelevant in this example because no onscreen videos are used
onscreen_gain = 0 # irrelevant in this example
SNR_increment = "0." # irrelevant in this example
beep = "" # path to beep.wav file ("" means no beep)
cont_mask = False # NOTE: important to leave this as false, as it would interfere with the audio/video sync 
ask_LE = False # whether to report listening effort; change as needed
n_LE = 0 # after how many sentences is listening effort reported (0=only at the end of a test condition)
change_vector = [1, 1, -1, -1] # irrelevant in this example because only 1 trial per condition
masker_duration = 3.52 # NOTE: make as long as needed (at least as long as the longest "target" audio file)
sync_background = 1 # NOTE: important that this is set to 1, to sync the background video with the audio files
info_msg = "This is a message that can be changed from the python script" # message that the experimenter should read out loud to the subject
active_screen = 0 # irrelevant
audio_only = 1 # NOTE: set to 1 because we don't want any video on the screens
spatialisation = 1

# Here we would set a list of target/onscreen sentences, but for this example we just use placeholders
# Note that there is no need to use empty wav files as target sentences. The Max patcher will send a message to the BiTA asking 
# to play some sources that do not exist in the XML file (e.g. source 7 when there are only 6 sources), but this message will be safely ignored
n_lists = 0 # 0 lists because we don't use target sentences
indexes = [] # this must be an empty array
listname = "Silent" # "Silent.txt" is an empty text file in the same folder as Control_patch.maxpat, and "Media/Silent" is just an empty folder
sentences = le.SentenceList(n_lists, indexes, listname)

# For all conditions, we consider 3 sources: 2 talkers at the left/right and 1 babble behind
nsources = 3
position_left = [1, 0, 45] # distance=1m, elevation=0deg, azimuth=45deg (left)
position_right = [1, 0, 315] # distance=1m, elevation=0deg, azimuth=315deg (right)
position_behind = [2, 0, 180] # distance=2m, elevation=0deg, azimuth=180deg (behind)

# With the above, we define our first test condition "Conversation1", which contains the masking condition "Conversation1_mc"
Conversation1_mc = le.MaskingCondition("Conversation1_mc", nsources) 
Conversation1_mc.set_source_positions([position_left,position_right,position_behind])
Conversation1_mc.set_audio_files([
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\ASL\\asl01s01.wav", # change as needed
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\ASL\\asl01s02.wav", # change as needed
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\Masking\\male_babble\\01Sp.wav" # change as needed
    ])
Conversation1_mc.set_video("D2_S1_Wedding.webm") # change as needed
Conversation1_mc.set_reverb_state(1) # optional
Conversation1 = le.TestCondition("Conversation1", Conversation1_mc, active_screen, audio_only, spatialisation, screens) # change masking condition and name as needed
Conversation1.description = "Conversation one: 2 talkers + 1 babble" # change description as needed

# At this point, we can define more conditions in the same way, changing the paths, names and descriptions as needed.
# Here we add a second condition, but more could be added in a similar way
Conversation2_mc = le.MaskingCondition("Conversation2_mc", nsources) 
Conversation2_mc.set_source_positions([position_left,position_right,position_behind])
Conversation2_mc.set_audio_files([
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\ASL\\asl01s03.wav", 
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\ASL\\asl01s04.wav",
    "C:\\Users\\Isaac\\Box Sync\\github\\listening_effort\\Media\\Masking\\male_babble\\01Sp.wav"
    ])
Conversation2_mc.set_video("D2_S1_Wedding.webm")
Conversation2_mc.set_reverb_state(1)
Conversation2 = le.TestCondition("Conversation2", Conversation2_mc, active_screen, audio_only, spatialisation, screens)
Conversation2.description = "Conversation two: 2 talkers + 1 babble"

# Create the test with the Test name and the list of test conditions. Leave all other parameters as they are.
test_one = le.Test("Pilot Conversation", [Conversation1,Conversation2], sentences, SNR_increment, beep, cont_mask, ask_LE, n_LE, change_vector, masker_duration, onscreen_gain, info_msg, sync_background)

# Finally output the XML and JSON files
test_one.output()

