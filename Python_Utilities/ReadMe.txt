Pilot_One_Multispeaker is an example script of working tests. It is commented to explain each step. It utilises the classes outlined in Test_Classes.py.

There are further scripts regarding content creations and graph creation in the respective folders.

For more information about the system as a whole please refer to the google doc.

To download the video assets, please find them in the box folder under assets
