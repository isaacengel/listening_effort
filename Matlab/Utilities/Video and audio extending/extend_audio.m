
% Extend all audio files in a folder by adding extra 1.5 seconds of
% silence at the beginning. Output folders are renamed as 'NAME_ext'.

% Audio folders (add as needed)
indirlist = {
    '../../../Media/ASL'
};

freezeTime = 1.5; % freezing time to be added at the beginning (s)

for dir_ind = 1:numel(indirlist)

    % Input and output video folders
    indir = indirlist{dir_ind};
    outdir = sprintf('%s_ext',indir);
    
    if ~isfolder(outdir)
        mkdir(outdir);
    end

    filelist = dir(indir);
    nfiles = numel(filelist)-2;
    
    fprintf('Processing folder %s. %d audio files found...\n',indir,nfiles)

    for i=3:nfiles+2 % skip . and ..
        filename = filelist(i).name;
        infilepath = [indir,'/',filename];
        outfilepath = [outdir,'/',filename];
        [x,fs] = audioread(infilepath); % read audio file
        nch = size(x,2); % number of channels
        silence = zeros(freezeTime*fs,nch);
        y = [silence;x]; % concatenate
        audiowrite(outfilepath,y,fs,'BitsPerSample',24);
        fprintf('%d/%d finished (%s)...\n',i-2,nfiles,outfilepath);
    end
end
fprintf('Done!\n')
