
inlets = 3;
outlets = 3;

setinletassist(0, "target index in");
setinletassist(1, "masker 1 index in");
setinletassist(2, "masker 2 index in");
setoutletassist(0, "target index out");
setoutletassist(1, "masker 1 index out");
setoutletassist(2, "masker 2 index out");

var indicesIn = [0, 0, 0];
var indicesOut = [1, 2, 3];

function msg_int(n) {
	
	// Asign new index
	if(n >= 1 && n <= 3) {
		indicesIn[inlet] = n;
		//post("assigning index n="+n+"to inlet "+inlet+"\n")
	} else {
	 	indicesIn[inlet] = 0;
		//post("invalid n="+n+",assigning random index to inlet "+inlet+"\n")
	}
	
	// If an index is repeated, remove the 2nd appearence onwards
	assigned = [0, 0, 0];
	for(i=0;i<3;i++){
		n = indicesIn[i];
		if(n>0 && assigned[n-1] == 0) {
			assigned[n-1] = 1;
			indicesOut[i] = n;
		} else {
			indicesOut[i] = 0;
		}
	}
	
	unassigned = new Array();
	for(i=0;i<3;i++){
		if(assigned[i]==0){
			unassigned.push(i+1);
		}
	}
	unassigned = shuffle(unassigned);
	
	// Asign remaining indices
	for(i=0;i<3;i++){
		if(indicesOut[i] == 0) {
			indicesOut[i] = unassigned.pop();
		}
	}
	
	
	outlet(0,indicesOut[0]);
	outlet(1,indicesOut[1]);
	outlet(2,indicesOut[2]);
}

function shuffle(array) {
  var currentIndex = array.length;
  var randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
  }

  return array;
}
